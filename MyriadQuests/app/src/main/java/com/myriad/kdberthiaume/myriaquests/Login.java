package com.myriad.kdberthiaume.myriaquests;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class Login extends Activity {

    EditText user_Name;
    EditText password;
    Button login_Button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user_Name=(EditText)this.findViewById(R.id.user_Name);
        password=(EditText)this.findViewById(R.id.password);
        login_Button=(Button)this.findViewById(R.id.login_Button);
        login_Button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if((user_Name.getText().equals(new String("Lancelot")) && password.getText().equals(new String("arthurDoesntknow")))){
                    Toast.makeText(Login.this, "Login Successful", Toast.LENGTH_LONG).show();
                } else{
                    Toast.makeText(Login.this, "Invalid Login",Toast.LENGTH_LONG).show();
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
